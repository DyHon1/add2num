class MyBigNumber {
  sum(stn1, stn2) {
    let result = "";
    let carry = 0;
    let maxLength = Math.max(stn1.length, stn2.length);
    // Duyet tung ki tu so trong chuoi.
    for (let i = 0; i < maxLength; i++) {
      let a = parseInt(stn1[stn1.length - 1 - i] || 0);
      let b = parseInt(stn2[stn2.length - 1 - i] || 0);
      let sum = a + b + carry;

      // Logging
      console.log(`Lay ${a} + ${b} + ${carry} = ${sum}, du ${Math.floor(sum / 10)}, viet ${sum % 10}`);
      
      result = (sum % 10) + result;
      carry = Math.floor(sum / 10);
    }
    // Truong hop 2 so co cung so ki tu va so du lon hon 0.
    if (carry > 0) {
      console.log(`Du: ${carry}, viet: ${carry}`);
      result = carry + result;
    }
    console.log(`Result: ${result}`);
    return result;
  }
}

module.exports = MyBigNumber;