# MyBigNumber

## Folder bao gồm:
- file index.html để test hàm.
- file MyBigNumber.js chứa class và hàm sum
- file test.js chứa các test unit, sử dụng thư viện assert của javascript, và chạy bằng mocha.

## Khởi chạy:

### 1. Cài đặt thư viện mocha.
`npm i`
### 2. Chạy file test.js bằng mocha.
`npx mocha test.js`

### Với file index.html, em đã bỏ hàm vô thẻ ạ. Có thể sử dụng extention live server của VSC để chạy trực tiếp file.

![Image](/screenshoot/1.png)


Em xin phép gửi anh,
