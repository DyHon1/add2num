const assert = require("assert");
const MyBigNumber = require("./MyBigNumber");

describe("MyBigNumber", function() {
  it("Phai cho gia tri chinh xac.", function() {
    const bigNumber = new MyBigNumber();
    const stn1 = "123";
    const stn2 = "456";
    const expectedResult = "579";
    const result = bigNumber.sum(stn1, stn2);
    assert.equal(result, expectedResult);
  });
  it("Cong duoc hai so khac so chu so.", function() {
    const bigNumber = new MyBigNumber();
    const stn1 = "123";
    const stn2 = "456789";
    const expectedResult = "456912";
    const result = bigNumber.sum(stn1, stn2);
    assert.equal(result, expectedResult);
  });

  it("Cong duoc hai so co gia tri lon.", function() {
    const bigNumber = new MyBigNumber();
    const stn1 = "9876543210";
    const stn2 = "1234567890";
    const expectedResult = "11111111100";
    const result = bigNumber.sum(stn1, stn2);
    assert.equal(result, expectedResult);
  });
});